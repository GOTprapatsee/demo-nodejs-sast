FROM node:19.2.0-alpine3.16

# Create app directory
WORKDIR /usr/src/app

COPY yarn.lock package.json ./

RUN yarn install

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "node", "server.js" ]